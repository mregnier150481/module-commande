<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230525141652 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE thematique_commande (thematique_id INT NOT NULL, commande_id INT NOT NULL, PRIMARY KEY(thematique_id, commande_id))');
        $this->addSql('CREATE INDEX IDX_2C630EF5476556AF ON thematique_commande (thematique_id)');
        $this->addSql('CREATE INDEX IDX_2C630EF582EA2E54 ON thematique_commande (commande_id)');
        $this->addSql('CREATE TABLE thematique_thematique (thematique_source INT NOT NULL, thematique_target INT NOT NULL, PRIMARY KEY(thematique_source, thematique_target))');
        $this->addSql('CREATE INDEX IDX_11D9AEB814E9E53 ON thematique_thematique (thematique_source)');
        $this->addSql('CREATE INDEX IDX_11D9AEB818ABCEDC ON thematique_thematique (thematique_target)');
        $this->addSql('ALTER TABLE thematique_commande ADD CONSTRAINT FK_2C630EF5476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE thematique_commande ADD CONSTRAINT FK_2C630EF582EA2E54 FOREIGN KEY (commande_id) REFERENCES commande (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE thematique_thematique ADD CONSTRAINT FK_11D9AEB814E9E53 FOREIGN KEY (thematique_source) REFERENCES thematique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE thematique_thematique ADD CONSTRAINT FK_11D9AEB818ABCEDC FOREIGN KEY (thematique_target) REFERENCES thematique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE thematique_commande DROP CONSTRAINT FK_2C630EF5476556AF');
        $this->addSql('ALTER TABLE thematique_commande DROP CONSTRAINT FK_2C630EF582EA2E54');
        $this->addSql('ALTER TABLE thematique_thematique DROP CONSTRAINT FK_11D9AEB814E9E53');
        $this->addSql('ALTER TABLE thematique_thematique DROP CONSTRAINT FK_11D9AEB818ABCEDC');
        $this->addSql('DROP TABLE thematique_commande');
        $this->addSql('DROP TABLE thematique_thematique');
    }
}

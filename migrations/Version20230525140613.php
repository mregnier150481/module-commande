<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230525140613 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE commentaire_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE commentaire (id INT NOT NULL, users_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, uptated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, commentaire TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_67F068BC67B3B43D ON commentaire (users_id)');
        $this->addSql('COMMENT ON COLUMN commentaire.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN commentaire.uptated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE user_budget (user_id INT NOT NULL, budget_id INT NOT NULL, PRIMARY KEY(user_id, budget_id))');
        $this->addSql('CREATE INDEX IDX_16A0EC46A76ED395 ON user_budget (user_id)');
        $this->addSql('CREATE INDEX IDX_16A0EC4636ABA6B8 ON user_budget (budget_id)');
        $this->addSql('CREATE TABLE user_thematique (user_id INT NOT NULL, thematique_id INT NOT NULL, PRIMARY KEY(user_id, thematique_id))');
        $this->addSql('CREATE INDEX IDX_9DE91344A76ED395 ON user_thematique (user_id)');
        $this->addSql('CREATE INDEX IDX_9DE91344476556AF ON user_thematique (thematique_id)');
        $this->addSql('CREATE TABLE user_commande (user_id INT NOT NULL, commande_id INT NOT NULL, PRIMARY KEY(user_id, commande_id))');
        $this->addSql('CREATE INDEX IDX_8E67427DA76ED395 ON user_commande (user_id)');
        $this->addSql('CREATE INDEX IDX_8E67427D82EA2E54 ON user_commande (commande_id)');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC67B3B43D FOREIGN KEY (users_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_budget ADD CONSTRAINT FK_16A0EC46A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_budget ADD CONSTRAINT FK_16A0EC4636ABA6B8 FOREIGN KEY (budget_id) REFERENCES budget (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_thematique ADD CONSTRAINT FK_9DE91344A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_thematique ADD CONSTRAINT FK_9DE91344476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_commande ADD CONSTRAINT FK_8E67427DA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_commande ADD CONSTRAINT FK_8E67427D82EA2E54 FOREIGN KEY (commande_id) REFERENCES commande (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE commentaire_id_seq CASCADE');
        $this->addSql('ALTER TABLE commentaire DROP CONSTRAINT FK_67F068BC67B3B43D');
        $this->addSql('ALTER TABLE user_budget DROP CONSTRAINT FK_16A0EC46A76ED395');
        $this->addSql('ALTER TABLE user_budget DROP CONSTRAINT FK_16A0EC4636ABA6B8');
        $this->addSql('ALTER TABLE user_thematique DROP CONSTRAINT FK_9DE91344A76ED395');
        $this->addSql('ALTER TABLE user_thematique DROP CONSTRAINT FK_9DE91344476556AF');
        $this->addSql('ALTER TABLE user_commande DROP CONSTRAINT FK_8E67427DA76ED395');
        $this->addSql('ALTER TABLE user_commande DROP CONSTRAINT FK_8E67427D82EA2E54');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE user_budget');
        $this->addSql('DROP TABLE user_thematique');
        $this->addSql('DROP TABLE user_commande');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230525141423 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE budget_exemplaire (budget_id INT NOT NULL, exemplaire_id INT NOT NULL, PRIMARY KEY(budget_id, exemplaire_id))');
        $this->addSql('CREATE INDEX IDX_88FFF3D236ABA6B8 ON budget_exemplaire (budget_id)');
        $this->addSql('CREATE INDEX IDX_88FFF3D25843AA21 ON budget_exemplaire (exemplaire_id)');
        $this->addSql('ALTER TABLE budget_exemplaire ADD CONSTRAINT FK_88FFF3D236ABA6B8 FOREIGN KEY (budget_id) REFERENCES budget (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE budget_exemplaire ADD CONSTRAINT FK_88FFF3D25843AA21 FOREIGN KEY (exemplaire_id) REFERENCES exemplaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE budget_exemplaire DROP CONSTRAINT FK_88FFF3D236ABA6B8');
        $this->addSql('ALTER TABLE budget_exemplaire DROP CONSTRAINT FK_88FFF3D25843AA21');
        $this->addSql('DROP TABLE budget_exemplaire');
    }
}

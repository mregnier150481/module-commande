<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230525141212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE exemplaire_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE budget_thematique (budget_id INT NOT NULL, thematique_id INT NOT NULL, PRIMARY KEY(budget_id, thematique_id))');
        $this->addSql('CREATE INDEX IDX_EC891AE836ABA6B8 ON budget_thematique (budget_id)');
        $this->addSql('CREATE INDEX IDX_EC891AE8476556AF ON budget_thematique (thematique_id)');
        $this->addSql('CREATE TABLE budget_commande (budget_id INT NOT NULL, commande_id INT NOT NULL, PRIMARY KEY(budget_id, commande_id))');
        $this->addSql('CREATE INDEX IDX_B925F7D936ABA6B8 ON budget_commande (budget_id)');
        $this->addSql('CREATE INDEX IDX_B925F7D982EA2E54 ON budget_commande (commande_id)');
        $this->addSql('CREATE TABLE exemplaire (id INT NOT NULL, name VARCHAR(255) NOT NULL, recu BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE budget_thematique ADD CONSTRAINT FK_EC891AE836ABA6B8 FOREIGN KEY (budget_id) REFERENCES budget (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE budget_thematique ADD CONSTRAINT FK_EC891AE8476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE budget_commande ADD CONSTRAINT FK_B925F7D936ABA6B8 FOREIGN KEY (budget_id) REFERENCES budget (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE budget_commande ADD CONSTRAINT FK_B925F7D982EA2E54 FOREIGN KEY (commande_id) REFERENCES commande (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE exemplaire_id_seq CASCADE');
        $this->addSql('ALTER TABLE budget_thematique DROP CONSTRAINT FK_EC891AE836ABA6B8');
        $this->addSql('ALTER TABLE budget_thematique DROP CONSTRAINT FK_EC891AE8476556AF');
        $this->addSql('ALTER TABLE budget_commande DROP CONSTRAINT FK_B925F7D936ABA6B8');
        $this->addSql('ALTER TABLE budget_commande DROP CONSTRAINT FK_B925F7D982EA2E54');
        $this->addSql('DROP TABLE budget_thematique');
        $this->addSql('DROP TABLE budget_commande');
        $this->addSql('DROP TABLE exemplaire');
    }
}

# Module commande

Organisation des commandes - gestion des budgets de la Médiathèque Jacques Chirac

## Environnement de développement

### Prérequis

* PHP 8.2
* Composer
* Symfony CLI
* Docker
* Docker-compose
* NodeJS & NPM

Vous pouvez vérifier les prérequis avec la commande suivante (sauf Docker et Docker-compose):

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony console doctrine:migrations:migrate
symfony serve -d
```

## Lancer les tests

```bash
make before-commit
```

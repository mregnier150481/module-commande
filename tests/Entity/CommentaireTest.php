<?php

namespace App\Tests\Entity;

use App\Entity\Commentaire;
use App\Entity\Exemplaire;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class CommentaireTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $commentaire = new Commentaire();
        $createdAt = new \DateTime();
        $updatedAt = new \DateTime();
        $comment = 'This is a test comment';
        $user = new User();
        $exemplaire = new Exemplaire();

        $commentaire->setCreatedAt($createdAt);
        $commentaire->setUptatedAt($updatedAt);
        $commentaire->setCommentaire($comment);
        $commentaire->setUsers($user);
        $commentaire->setExemplaire($exemplaire);

        $this->assertEquals(null, $commentaire->getId());
        $this->assertEquals($createdAt, $commentaire->getCreatedAt());
        $this->assertEquals($updatedAt, $commentaire->getUptatedAt());
        $this->assertEquals($comment, $commentaire->getCommentaire());
        $this->assertEquals($user, $commentaire->getUsers());
        $this->assertEquals($exemplaire, $commentaire->getExemplaire());
    }
}
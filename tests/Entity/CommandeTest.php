<?php

namespace App\Tests\Entity;

use App\Entity\Commande;
use App\Entity\Budget;
use App\Entity\Exemplaire;
use App\Entity\Thematique;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class CommandeTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $commande = new Commande();

        $this->assertNull($commande->getId());
        $this->assertNull($commande->getName());
        $this->assertNull($commande->getCreatedAt());

        $commande->setName('Test Commande');
        $this->assertSame('Test Commande', $commande->getName());

        $createdAt = new \DateTime();
        $commande->setCreatedAt($createdAt);
        $this->assertSame($createdAt, $commande->getCreatedAt());
    }

    public function testUsers(): void
    {
        $commande = new Commande();
        $user1 = new User();
        $user2 = new User();

        $this->assertCount(0, $commande->getUsers());

        $commande->addUser($user1);
        $this->assertCount(1, $commande->getUsers());
        $this->assertTrue($commande->getUsers()->contains($user1));

        $commande->addUser($user2);
        $this->assertCount(2, $commande->getUsers());
        $this->assertTrue($commande->getUsers()->contains($user2));

        $commande->removeUser($user1);
        $this->assertCount(1, $commande->getUsers());
        $this->assertFalse($commande->getUsers()->contains($user1));
    }

    public function testBudgets(): void
    {
        $commande = new Commande();
        $budget1 = new Budget();
        $budget2 = new Budget();

        $this->assertCount(0, $commande->getBudgets());

        $commande->addBudget($budget1);
        $this->assertCount(1, $commande->getBudgets());
        $this->assertTrue($commande->getBudgets()->contains($budget1));

        $commande->addBudget($budget2);
        $this->assertCount(2, $commande->getBudgets());
        $this->assertTrue($commande->getBudgets()->contains($budget2));

        $commande->removeBudget($budget1);
        $this->assertCount(1, $commande->getBudgets());
        $this->assertFalse($commande->getBudgets()->contains($budget1));
    }

    public function testThematiques(): void
    {
        $commande = new Commande();
        $thematique1 = new Thematique();
        $thematique2 = new Thematique();

        $this->assertCount(0, $commande->getThematiques());

        $commande->addThematique($thematique1);
        $this->assertCount(1, $commande->getThematiques());
        $this->assertTrue($commande->getThematiques()->contains($thematique1));

        $commande->addThematique($thematique2);
        $this->assertCount(2, $commande->getThematiques());
        $this->assertTrue($commande->getThematiques()->contains($thematique2));

        $commande->removeThematique($thematique1);
        $this->assertCount(1, $commande->getThematiques());
        $this->assertFalse($commande->getThematiques()->contains($thematique1));
    }

    public function testExemplaires(): void
    {
        $commande = new Commande();
        $exemplaire1 = new Exemplaire();
        $exemplaire2 = new Exemplaire();

        $this->assertCount(0, $commande->getExemplaires());

        $commande->addExemplaire($exemplaire1);
        $this->assertCount(1, $commande->getExemplaires());
        $this->assertTrue($commande->getExemplaires()->contains($exemplaire1));

        $commande->addExemplaire($exemplaire2);
        $this->assertCount(2, $commande->getExemplaires());
        $this->assertTrue($commande->getExemplaires()->contains($exemplaire2));

        $commande->removeExemplaire($exemplaire1);
        $this->assertCount(1, $commande->getExemplaires());
        $this->assertFalse($commande->getExemplaires()->contains($exemplaire1));
    }
}
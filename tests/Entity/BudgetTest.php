<?php

namespace App\Tests\Entity;

use App\Entity\Budget;
use App\Entity\Commande;
use App\Entity\Exemplaire;
use App\Entity\Thematique;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class BudgetTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $budget = new Budget();

        $budget->setName('Test Budget');
        $this->assertEquals('Test Budget', $budget->getName());

        $budget->setMontantInitial(100.0);
        $this->assertEquals(100.0, $budget->getMontantInitial());

        $budget->setMontantEngagé(50.0);
        $this->assertEquals(50.0, $budget->getMontantEngagé());

        $budget->setMontantFacturé(25.0);
        $this->assertEquals(25.0, $budget->getMontantFacturé());
    }

    public function testManyToManyRelationshipsWithUser(): void
    {
        $budget = new Budget();
        $user1 = new User();
        $user2 = new User();

        $budget->addUser($user1);
        $budget->addUser($user2);

        $this->assertCount(2, $budget->getUsers());
        $this->assertTrue($budget->getUsers()->contains($user1));
        $this->assertTrue($budget->getUsers()->contains($user2));

        $budget->removeUser($user1);

        $this->assertCount(1, $budget->getUsers());
        $this->assertFalse($budget->getUsers()->contains($user1));
        $this->assertTrue($budget->getUsers()->contains($user2));
    }

    public function testManyToManyRelationshipsWithThematique(): void
    {
        $budget = new Budget();
        $thematique1 = new Thematique();
        $thematique2 = new Thematique();

        $budget->addThematique($thematique1);
        $budget->addThematique($thematique2);

        $this->assertCount(2, $budget->getThematique());
        $this->assertTrue($budget->getThematique()->contains($thematique1));
        $this->assertTrue($budget->getThematique()->contains($thematique2));

        $budget->removeThematique($thematique1);

        $this->assertCount(1, $budget->getThematique());
        $this->assertFalse($budget->getThematique()->contains($thematique1));
        $this->assertTrue($budget->getThematique()->contains($thematique2));
    }

    public function testManyToManyRelationshipsWithCommande(): void
    {
        $budget = new Budget();
        $commande1 = new Commande();
        $commande2 = new Commande();

        $budget->addCommande($commande1);
        $budget->addCommande($commande2);

        $this->assertCount(2, $budget->getCommandes());
        $this->assertTrue($budget->getCommandes()->contains($commande1));
        $this->assertTrue($budget->getCommandes()->contains($commande2));

        $budget->removeCommande($commande1);

        $this->assertCount(1, $budget->getCommandes());
        $this->assertFalse($budget->getCommandes()->contains($commande1));
        $this->assertTrue($budget->getCommandes()->contains($commande2));
    }

    public function testManyToManyRelationshipsWithExemplaire(): void
    {
        $budget = new Budget();
        $exemplaire1 = new Exemplaire();
        $exemplaire2 = new Exemplaire();

        $budget->addExemplaire($exemplaire1);
        $budget->addExemplaire($exemplaire2);

        $this->assertCount(2, $budget->getExemplaire());
        $this->assertTrue($budget->getExemplaire()->contains($exemplaire1));
        $this->assertTrue($budget->getExemplaire()->contains($exemplaire2));

        $budget->removeExemplaire($exemplaire1);

        $this->assertCount(1, $budget->getExemplaire());
        $this->assertFalse($budget->getExemplaire()->contains($exemplaire1));
        $this->assertTrue($budget->getExemplaire()->contains($exemplaire2));
    }
}
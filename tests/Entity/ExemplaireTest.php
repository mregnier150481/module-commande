<?php

namespace App\Tests\Entity;

use App\Entity\Budget;
use App\Entity\Commande;
use App\Entity\Commentaire;
use App\Entity\Document;
use App\Entity\Exemplaire;
use PHPUnit\Framework\TestCase;

class ExemplaireTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $exemplaire = new Exemplaire();

        $this->assertNull($exemplaire->getId());
        $this->assertNull($exemplaire->getName());
        $this->assertNull($exemplaire->isRecu());
        $this->assertNull($exemplaire->getCommande());
        $this->assertNull($exemplaire->getDocument());

        $exemplaire->setName('Test Exemplaire');
        $this->assertSame('Test Exemplaire', $exemplaire->getName());

        $exemplaire->setRecu(true);
        $this->assertTrue($exemplaire->isRecu());

        $commande = new Commande();
        $exemplaire->setCommande($commande);
        $this->assertSame($commande, $exemplaire->getCommande());

        $document = new Document();
        $exemplaire->setDocument($document);
        $this->assertSame($document, $exemplaire->getDocument());
    }

    public function testBudgets(): void
    {
        $exemplaire = new Exemplaire();
        $budget1 = new Budget();
        $budget2 = new Budget();

        $this->assertCount(0, $exemplaire->getBudgets());

        $exemplaire->addBudget($budget1);
        $this->assertCount(1, $exemplaire->getBudgets());
        $this->assertTrue($exemplaire->getBudgets()->contains($budget1));
        $this->assertTrue($budget1->getExemplaire()->contains($exemplaire));

        $exemplaire->addBudget($budget2);
        $this->assertCount(2, $exemplaire->getBudgets());
        $this->assertTrue($exemplaire->getBudgets()->contains($budget2));
        $this->assertTrue($budget2->getExemplaire()->contains($exemplaire));

        $exemplaire->removeBudget($budget1);
        $this->assertCount(1, $exemplaire->getBudgets());
        $this->assertFalse($exemplaire->getBudgets()->contains($budget1));
        $this->assertFalse($budget1->getExemplaire()->contains($exemplaire));

        $exemplaire->removeBudget($budget2);
        $this->assertCount(0, $exemplaire->getBudgets());
        $this->assertFalse($exemplaire->getBudgets()->contains($budget2));
        $this->assertFalse($budget2->getExemplaire()->contains($exemplaire));
    }

    public function testCommentaires(): void
    {
        $exemplaire = new Exemplaire();
        $commentaire1 = new Commentaire();
        $commentaire2 = new Commentaire();

        $this->assertCount(0, $exemplaire->getCommentaires());

        $exemplaire->addCommentaire($commentaire1);
        $this->assertCount(1, $exemplaire->getCommentaires());
        $this->assertTrue($exemplaire->getCommentaires()->contains($commentaire1));
        $this->assertSame($exemplaire, $commentaire1->getExemplaire());

        $exemplaire->addCommentaire($commentaire2);
        $this->assertCount(2, $exemplaire->getCommentaires());
        $this->assertTrue($exemplaire->getCommentaires()->contains($commentaire2));
        $this->assertSame($exemplaire, $commentaire2->getExemplaire());

        $exemplaire->removeCommentaire($commentaire1);
        $this->assertCount(1, $exemplaire->getCommentaires());
        $this->assertFalse($exemplaire->getCommentaires()->contains($commentaire1));
        $this->assertNull($commentaire1->getExemplaire());

        $exemplaire->removeCommentaire($commentaire2);
        $this->assertCount(0, $exemplaire->getCommentaires());
        $this->assertFalse($exemplaire->getCommentaires()->contains($commentaire2));
        $this->assertNull($commentaire2->getExemplaire());
    }
}
<?php

namespace App\Tests\Entity;

use App\Entity\Document;
use PHPUnit\Framework\TestCase;

class DocumentTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $document = new Document();

        $document->setEAN('1234567890123');
        $this->assertEquals('1234567890123', $document->getEAN());

        $document->setTitre('Test Title');
        $this->assertEquals('Test Title', $document->getTitre());

        $document->setAuteur('Test Author');
        $this->assertEquals('Test Author', $document->getAuteur());

        $document->setPrix(9.99);
        $this->assertEquals(9.99, $document->getPrix());

        $document->setTVA('20%');
        $this->assertEquals('20%', $document->getTVA());
    }

    public function testAddAndRemoveExemplaire()
    {
        $document = new Document();
        $exemplaire = $this->createMock('App\Entity\Exemplaire');

        $document->addExemplaire($exemplaire);
        $this->assertTrue($document->getExemplaires()->contains($exemplaire));

        $document->removeExemplaire($exemplaire);
        $this->assertFalse($document->getExemplaires()->contains($exemplaire));
    }
}
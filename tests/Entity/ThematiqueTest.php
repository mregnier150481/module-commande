<?php

namespace App\Tests\Entity;

use App\Entity\Budget;
use App\Entity\Commande;
use App\Entity\Thematique;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ThematiqueTest extends TestCase
{
    private Thematique $thematique;

    protected function setUp(): void
    {
        $this->thematique = new Thematique();
    }

    public function testId(): void
    {
        $this->assertNull($this->thematique->getId());
    }

    public function testName(): void
    {
        $name = 'Test Thematique';
        $this->thematique->setName($name);
        $this->assertEquals($name, $this->thematique->getName());
    }

    public function testUsers(): void
    {
        $user1 = new User();
        $user2 = new User();

        $this->thematique->addUser($user1);
        $this->thematique->addUser($user2);

        $this->assertCount(2, $this->thematique->getUsers());

        $this->thematique->removeUser($user1);

        $this->assertCount(1, $this->thematique->getUsers());
    }

    public function testBudgets(): void
    {
        $budget1 = new Budget();
        $budget2 = new Budget();

        $this->thematique->addBudget($budget1);
        $this->thematique->addBudget($budget2);

        $this->assertCount(2, $this->thematique->getBudgets());

        $this->thematique->removeBudget($budget1);

        $this->assertCount(1, $this->thematique->getBudgets());
    }

    public function testCommandes(): void
    {
        $commande1 = new Commande();
        $commande2 = new Commande();

        $this->thematique->addCommande($commande1);
        $this->thematique->addCommande($commande2);

        $this->assertCount(2, $this->thematique->getCommandes());

        $this->thematique->removeCommande($commande1);

        $this->assertCount(1, $this->thematique->getCommandes());
    }

    public function testExemplaires(): void
    {
        $exemplaire1 = new Thematique();
        $exemplaire2 = new Thematique();

        $this->thematique->addExemplaire($exemplaire1);
        $this->thematique->addExemplaire($exemplaire2);

        $this->assertCount(2, $this->thematique->getExemplaires());

        $this->thematique->removeExemplaire($exemplaire1);

        $this->assertCount(1, $this->thematique->getExemplaires());
    }

    public function testThematiques(): void
    {
        $thematique1 = new Thematique();
        $thematique2 = new Thematique();

        $this->thematique->addThematique($thematique1);
        $this->thematique->addThematique($thematique2);

        $this->assertCount(2, $this->thematique->getThematiques());

        $this->thematique->removeThematique($thematique1);

        $this->assertCount(1, $this->thematique->getThematiques());
    }
}
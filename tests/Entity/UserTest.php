<?php
namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testGetId(): void
    {
        $user = new User();
        $this->assertNull($user->getId());
    }

    public function testGetEmail(): void
    {
        $user = new User();
        $user->setEmail('test@example.com');
        $this->assertSame('test@example.com', $user->getEmail());
    }

    public function testGetRoles(): void
    {
        $user = new User();
        $this->assertContains('ROLE_USER', $user->getRoles());
    }

    public function testSetRoles(): void
    {
        $user = new User();
        $user->setRoles(['ROLE_ADMIN']);
        $this->assertContains('ROLE_ADMIN', $user->getRoles());
    }

    public function testGetPassword(): void
    {
        $user = new User();
        $user->setPassword('password');
        $this->assertSame('password', $user->getPassword());
    }

    public function testGetCommentaires(): void
    {
        $user = new User();
        $this->assertInstanceOf('Doctrine\Common\Collections\Collection', $user->getCommentaires());
    }

    public function testAddCommentaire(): void
    {
        $user = new User();
        $commentaire = $this->createMock('App\Entity\Commentaire');
        $user->addCommentaire($commentaire);
        $this->assertTrue($user->getCommentaires()->contains($commentaire));
    }

    public function testRemoveCommentaire(): void
    {
        $user = new User();
        $commentaire = $this->createMock('App\Entity\Commentaire');
        $user->addCommentaire($commentaire);
        $user->removeCommentaire($commentaire);
        $this->assertFalse($user->getCommentaires()->contains($commentaire));
    }

    public function testGetBudgets(): void
    {
        $user = new User();
        $this->assertInstanceOf('Doctrine\Common\Collections\Collection', $user->getBudgets());
    }

    public function testAddBudget(): void
    {
        $user = new User();
        $budget = $this->createMock('App\Entity\Budget');
        $user->addBudget($budget);
        $this->assertTrue($user->getBudgets()->contains($budget));
    }

    public function testRemoveBudget(): void
    {
        $user = new User();
        $budget = $this->createMock('App\Entity\Budget');
        $user->addBudget($budget);
        $user->removeBudget($budget);
        $this->assertFalse($user->getBudgets()->contains($budget));
    }

    public function testGetThematiques(): void
    {
        $user = new User();
        $this->assertInstanceOf('Doctrine\Common\Collections\Collection', $user->getThematiques());
    }

    public function testAddThematique(): void
    {
        $user = new User();
        $thematique = $this->createMock('App\Entity\Thematique');
        $user->addThematique($thematique);
        $this->assertTrue($user->getThematiques()->contains($thematique));
    }

    public function testRemoveThematique(): void
    {
        $user = new User();
        $thematique = $this->createMock('App\Entity\Thematique');
        $user->addThematique($thematique);
        $user->removeThematique($thematique);
        $this->assertFalse($user->getThematiques()->contains($thematique));
    }

    public function testGetCommandes(): void
    {
        $user = new User();
        $this->assertInstanceOf('Doctrine\Common\Collections\Collection', $user->getCommandes());
    }

    public function testAddCommande(): void
    {
        $user = new User();
        $commande = $this->createMock('App\Entity\Commande');
        $user->addCommande($commande);
        $this->assertTrue($user->getCommandes()->contains($commande));
    }

    public function testRemoveCommande(): void
    {
        $user = new User();
        $commande = $this->createMock('App\Entity\Commande');
        $user->addCommande($commande);
        $user->removeCommande($commande);
        $this->assertFalse($user->getCommandes()->contains($commande));
    }
}
<?php

namespace App\Entity;

use App\Repository\ThematiqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ThematiqueRepository::class)]
class Thematique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /**
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'thematiques')]
    private Collection $users;

    /**
     * @var Collection<int, Budget>
     */
    #[ORM\ManyToMany(targetEntity: Budget::class, mappedBy: 'thematique')]
    private Collection $budgets;

    /**
     * @var Collection<int, Commande>
     */
    #[ORM\ManyToMany(targetEntity: Commande::class, inversedBy: 'thematiques')]
    private Collection $Commandes;

    /**
     * @var Collection<int, self>
     */
    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'thematiques')]
    private Collection $exemplaires;

    /**
     * @var Collection<int, self>
     */
    #[ORM\ManyToMany(targetEntity: self::class, mappedBy: 'exemplaires')]
    private Collection $thematiques;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->budgets = new ArrayCollection();
        $this->Commandes = new ArrayCollection();
        $this->exemplaires = new ArrayCollection();
        $this->thematiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addThematique($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeThematique($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Budget>
     */
    public function getBudgets(): Collection
    {
        return $this->budgets;
    }

    public function addBudget(Budget $budget): self
    {
        if (!$this->budgets->contains($budget)) {
            $this->budgets->add($budget);
            $budget->addThematique($this);
        }

        return $this;
    }

    public function removeBudget(Budget $budget): self
    {
        if ($this->budgets->removeElement($budget)) {
            $budget->removeThematique($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Commande>
     */
    public function getCommandes(): Collection
    {
        return $this->Commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->Commandes->contains($commande)) {
            $this->Commandes->add($commande);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        $this->Commandes->removeElement($commande);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getExemplaires(): Collection
    {
        return $this->exemplaires;
    }

    public function addExemplaire(self $exemplaire): self
    {
        if (!$this->exemplaires->contains($exemplaire)) {
            $this->exemplaires->add($exemplaire);
        }

        return $this;
    }

    public function removeExemplaire(self $exemplaire): self
    {
        $this->exemplaires->removeElement($exemplaire);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getThematiques(): Collection
    {
        return $this->thematiques;
    }

    public function addThematique(self $thematique): self
    {
        if (!$this->thematiques->contains($thematique)) {
            $this->thematiques->add($thematique);
            $thematique->addExemplaire($this);
        }

        return $this;
    }

    public function removeThematique(self $thematique): self
    {
        if ($this->thematiques->removeElement($thematique)) {
            $thematique->removeExemplaire($this);
        }

        return $this;
    }
}

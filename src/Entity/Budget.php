<?php

namespace App\Entity;

use App\Repository\BudgetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BudgetRepository::class)]
class Budget
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?float $montantInitial = null;

    #[ORM\Column]
    private ?float $montantEngagé = null;

    #[ORM\Column]
    private ?float $montantFacturé = null;

    /**
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'budgets')]
    private Collection $users;

    /**
     * @var Collection<int, Thematique>
     */
    #[ORM\ManyToMany(targetEntity: Thematique::class, inversedBy: 'budgets')]
    private Collection $thematique;

    /**
     * @var Collection<int, Commande>
     */
    #[ORM\ManyToMany(targetEntity: Commande::class, inversedBy: 'budgets')]
    private Collection $commandes;

    /**
     * @var Collection<int, Exemplaire>
     */
    #[ORM\ManyToMany(targetEntity: Exemplaire::class, inversedBy: 'budgets')]
    private Collection $exemplaire;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->thematique = new ArrayCollection();
        $this->commandes = new ArrayCollection();
        $this->exemplaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMontantInitial(): ?float
    {
        return $this->montantInitial;
    }

    public function setMontantInitial(float $montantInitial): self
    {
        $this->montantInitial = $montantInitial;

        return $this;
    }

    public function getMontantEngagé(): ?float
    {
        return $this->montantEngagé;
    }

    public function setMontantEngagé(float $montantEngagé): self
    {
        $this->montantEngagé = $montantEngagé;

        return $this;
    }

    public function getMontantFacturé(): ?float
    {
        return $this->montantFacturé;
    }

    public function setMontantFacturé(float $montantFacturé): self
    {
        $this->montantFacturé = $montantFacturé;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addBudget($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeBudget($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Thematique>
     */
    public function getThematique(): Collection
    {
        return $this->thematique;
    }

    public function addThematique(Thematique $thematique): self
    {
        if (!$this->thematique->contains($thematique)) {
            $this->thematique->add($thematique);
        }

        return $this;
    }

    public function removeThematique(Thematique $thematique): self
    {
        $this->thematique->removeElement($thematique);

        return $this;
    }

    /**
     * @return Collection<int, Commande>
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes->add($commande);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        $this->commandes->removeElement($commande);

        return $this;
    }

    /**
     * @return Collection<int, Exemplaire>
     */
    public function getExemplaire(): Collection
    {
        return $this->exemplaire;
    }

    public function addExemplaire(Exemplaire $exemplaire): self
    {
        if (!$this->exemplaire->contains($exemplaire)) {
            $this->exemplaire->add($exemplaire);
        }

        return $this;
    }

    public function removeExemplaire(Exemplaire $exemplaire): self
    {
        $this->exemplaire->removeElement($exemplaire);

        return $this;
    }
}
